

import os
import json


def get_documents(source):
    full_path = os.getcwd() + "/data/" + source
    print "Retrieving docs from {}".format(full_path)
    source_file = open(full_path)
    jformatted_lines = source_file.readlines()
    json_docs = [json.loads(line) for line in jformatted_lines]
    return json_docs

def get_search_on_surface_form(source, surface_form):
    documents = get_documents(source)
    documents_filtered = [doc for doc in documents if doc["t"].startswith(surface_form)]
    return documents_filtered

def get_search_on_doc_id(source, surface_form):
    documents = get_documents(source)
    documents_filtered = [doc for doc in documents if doc["doc_id"].startswith(surface_form)]
    return documents_filtered

def get_document_by_id(source, id):
    documents = get_documents(source)
    document = [doc for doc in documents if doc["id"].startswith(id)]
    return document