#!/usr/bin/env python
from flask import Flask
from flask import request
import json
from flask import render_template
import file_accessor as fa
import dev_search_client as devc

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html') 

@app.route('/mentions/', methods=["GET","POST"])
def mentions_list():
    surfaceform = request.form.get("surfaceform")
    if not surfaceform:
        surfaceform = ""
    paging = request.form.get("paging")
    if not paging:
        paging = 0
    else:
        paging = int(paging)

    jdocs = fa.get_search_on_surface_form('display-extracted-sentences-mentions.json', surfaceform)

    txt = "{} documents found for surface form: {}".format(str(len(jdocs)), surfaceform)
    return render_template('mentions.html', jdocs=jdocs[paging:300+paging], txt=txt, surfaceform=surfaceform, paging=paging)

@app.route('/entities/', methods=["GET","POST"])
def entities_list():
    surfaceform = request.form.get("surfaceform")
    if not surfaceform:
        surfaceform = ""
    paging = request.form.get("paging")
    if not paging:
        paging = 0
    else:
        paging = int(paging)

    jdocs = fa.get_search_on_surface_form('display-extracted-sentences-entities.json', surfaceform)

    txt = "{} documents found for surface form: {}".format(str(len(jdocs)), surfaceform)
    return render_template('entities.html', jdocs=jdocs[paging:300+paging], txt=txt, surfaceform=surfaceform, paging=paging)

@app.route('/documents/', methods=["GET","POST"])
def documents_list():
    surfaceform = request.form.get("surfaceform")
    if not surfaceform:
        surfaceform = ""
    paging = request.form.get("paging")
    if not paging:
        paging = 0
    else:
        paging = int(paging)

    jdocs = fa.get_search_on_doc_id('ConsolidatedDocs.json', surfaceform)

    txt = "{} documents found for surface form: {}".format(str(len(jdocs)), surfaceform)
    return render_template('documents.html', jdocs=jdocs[paging:10+paging], txt=txt, surfaceform=surfaceform, paging=paging)


@app.route('/entitiesmed/', methods=["GET","POST"])
def entitiesmed_list():
    surfaceform = request.form.get("surfaceform")
    if not surfaceform:
        surfaceform = ""
    paging = request.form.get("paging")
    if not paging:
        paging = 0
    else:
        paging = int(paging)

    jdocs = fa.get_search_on_surface_form('display-entities-medical.json', surfaceform)

    txt = "{} documents found for surface form: {}".format(str(len(jdocs)), surfaceform)
    return render_template('entitiesmed.html', jdocs=jdocs[paging:300+paging], txt=txt, surfaceform=surfaceform, paging=paging)

@app.route('/mentionsmed/', methods=["GET","POST"])
def mentionsmed_list():
    surfaceform = request.form.get("surfaceform")
    if not surfaceform:
        surfaceform = ""
    paging = request.form.get("paging")
    if not paging:
        paging = 0
    else:
        paging = int(paging)

    jdocs = fa.get_search_on_surface_form('display-mentions-medical.json', surfaceform)

    txt = "{} documents found for surface form: {}".format(str(len(jdocs)), surfaceform)
    return render_template('mentionsmed.html', jdocs=jdocs[paging:300+paging], txt=txt, surfaceform=surfaceform, paging=paging)

if __name__ == "__main__":
    app.run(
            '0.0.0.0', # leave out for local, use 0.0.0.0 to open to network
            8080,      # optional: set the port, default is 5000)
            debug=True # optional: starts an interactive web debugger on exception
            )
