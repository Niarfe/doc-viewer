import requests
import json





def get_tawgs(doc_id):
    try:
        response = requests.get("http://dev-search-api:80/docs/{}".format(doc_id))
    except:
        print "NO DEV_SEARCH CONNECTION - returning empty dictionaries for tawgs"
        return []

    jobj = json.loads(response.text)
    try:
        return jobj["doc"]["tawgs"]
    except:
        return []


print get_tawgs("e3faf3956e95613c")
