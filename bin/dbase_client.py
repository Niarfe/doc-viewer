#!/usr/bin/env python
import requests
import couchdb
import os
import json

os.system("curl -X PUT http://localhost:5984/entities")

couch = couchdb.Server("http://localhost:5984")
db = couch["entities"]
print "Begin load target file to database"
with open('../data/display-document-entities-and-mentions.json') as source_file:
    for row in source_file:
        jobj_doc = json.loads(row)
        jobj_doc["_id"] = jobj_doc["doc_id"]
        db.save(jobj_doc)
print "Finished loading to database"



#os.system("curl -X DELETE http://localhost:5984/entities") 
#curl -X GET http://localhost:5984/_all_dbs
